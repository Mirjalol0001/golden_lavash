import os.path
from googleapiclient.discovery import build
from google.oauth2 import service_account

SCOPES = ['https://www.googleapis.com/auth/spreadsheets']
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
SERVICE_ACCOUNT_FILE = os.path.join(BASE_DIR, 'creds.json')

credentials = service_account.Credentials.from_service_account_file(
    SERVICE_ACCOUNT_FILE, scopes=SCOPES)

# The ID and range of a sample spreadsheet.
SAMPLE_SPREADSHEET_ID = '1yeCtQsNxVM5L98L1bwm3Ug35Jiw57Y9NR9MEhh1XhKw'
SAMPLE_RANGE_NAME = 'Пита день-03'
SAMPLE_RANGE_NAME_SECOND = 'Булочка день-03'
SAMPLE_RANGE_NAME_THIRD = 'Лаваш день-03'
SAMPLE_RANGE_NAME_FOURTH = 'Пита ночь-03'
SAMPLE_RANGE_NAME_FIFTH = 'Булочка ночь-03'
SAMPLE_RANGE_NAME_SIXTH = 'Лаваш ночь-03'
service_first = build('sheets', 'v4', credentials=credentials).spreadsheets().values()

result_first = service_first.get(spreadsheetId=SAMPLE_SPREADSHEET_ID,
                                 range=SAMPLE_RANGE_NAME).execute()
result_second = service_first.get(spreadsheetId=SAMPLE_SPREADSHEET_ID,
                                  range=SAMPLE_RANGE_NAME_SECOND).execute()
result_third = service_first.get(spreadsheetId=SAMPLE_SPREADSHEET_ID,
                                 range=SAMPLE_RANGE_NAME_THIRD).execute()
result_fourth = service_first.get(spreadsheetId=SAMPLE_SPREADSHEET_ID,
                                  range=SAMPLE_RANGE_NAME_FOURTH).execute()
result_fifth = service_first.get(spreadsheetId=SAMPLE_SPREADSHEET_ID,
                                 range=SAMPLE_RANGE_NAME_FIFTH).execute()
result_sixth = service_first.get(spreadsheetId=SAMPLE_SPREADSHEET_ID,
                                 range=SAMPLE_RANGE_NAME_SIXTH).execute()
# data_from_sheet = result_first.get('values', [])

# print(result_second['values'][21])

# working_staff = {
#     result_first['values'][0][1]: [
#         {
#             'name': result_first['values'][1][1],
#             'dates': [
#                 {
#                     'date': result_first['values'][0][2],
#                     'value': result_first['values'][1][2]
#                 },
#                 {
#                     'date': result_first['values'][0][3],
#                     'value': result_first['values'][1][3]
#                 },
#                 {
#                     'date': result_first['values'][0][4],
#                     'value': result_first['values'][1][4]
#                 },
#                 {
#                     'date': result_first['values'][0][5],
#                     'value': result_first['values'][1][5]
#                 },
#                 {
#                     'date': result_first['values'][0][6],
#                     'value': result_first['values'][1][6]
#                 },
#                 {
#                     'date': result_first['values'][0][7],
#                     'value': result_first['values'][1][7]
#                 },
#                 {
#                     'date': result_first['values'][0][8],
#                     'value': result_first['values'][1][8]
#                 },
#                 {
#                     'date': result_first['values'][0][9],
#                     'value': result_first['values'][1][9]
#                 },
#                 {
#                     'date': result_first['values'][0][10],
#                     'value': result_first['values'][1][10]
#                 },
#                 {
#                     'date': result_first['values'][0][11],
#                     'value': result_first['values'][1][11]
#                 },
#                 {
#                     'date': result_first['values'][0][12],
#                     'value': result_first['values'][1][12]
#                 },
#                 {
#                     'date': result_first['values'][0][13],
#                     'value': result_first['values'][1][13]
#                 },
#                 {
#                     'date': result_first['values'][0][14],
#                     'value': result_first['values'][1][14]
#                 },
#                 {
#                     'date': result_first['values'][0][15],
#                     'value': result_first['values'][1][15]
#                 },
#             ]
#         },
#         {
#             'name': result_first['values'][2][1],
#             'dates': [
#                 {
#                     'date': result_first['values'][0][2],
#                     'value': result_first['values'][2][2]
#                 },
#                 {
#                     'date': result_first['values'][0][3],
#                     'value': result_first['values'][2][3]
#                 },
#                 {
#                     'date': result_first['values'][0][4],
#                     'value': result_first['values'][2][4]
#                 },
#                 {
#                     'date': result_first['values'][0][5],
#                     'value': result_first['values'][2][5]
#                 },
#                 {
#                     'date': result_first['values'][0][6],
#                     'value': result_first['values'][2][6]
#                 },
#                 {
#                     'date': result_first['values'][0][7],
#                     'value': result_first['values'][2][7]
#                 },
#                 {
#                     'date': result_first['values'][0][8],
#                     'value': result_first['values'][2][8]
#                 },
#                 {
#                     'date': result_first['values'][0][9],
#                     'value': result_first['values'][2][9]
#                 },
#                 {
#                     'date': result_first['values'][0][10],
#                     'value': result_first['values'][2][10]
#                 },
#                 {
#                     'date': result_first['values'][0][11],
#                     'value': result_first['values'][2][11]
#                 },
#                 {
#                     'date': result_first['values'][0][12],
#                     'value': result_first['values'][2][12]
#                 },
#                 {
#                     'date': result_first['values'][0][13],
#                     'value': result_first['values'][2][13]
#                 },
#                 {
#                     'date': result_first['values'][0][14],
#                     'value': result_first['values'][2][14]
#                 },
#                 {
#                     'date': result_first['values'][0][15],
#                     'value': result_first['values'][2][15]
#                 },
#             ]
#         },
#         {
#             'name': result_first['values'][3][1],
#             'dates': [
#                 {
#                     'date': result_first['values'][0][2],
#                     'value': result_first['values'][3][2]
#                 },
#                 {
#                     'date': result_first['values'][0][3],
#                     'value': result_first['values'][3][3]
#                 },
#                 {
#                     'date': result_first['values'][0][4],
#                     'value': result_first['values'][3][4]
#                 },
#                 {
#                     'date': result_first['values'][0][5],
#                     'value': result_first['values'][3][5]
#                 },
#                 {
#                     'date': result_first['values'][0][6],
#                     'value': result_first['values'][3][6]
#                 },
#                 {
#                     'date': result_first['values'][0][7],
#                     'value': result_first['values'][3][7]
#                 },
#                 {
#                     'date': result_first['values'][0][8],
#                     'value': result_first['values'][3][8]
#                 },
#                 {
#                     'date': result_first['values'][0][9],
#                     'value': result_first['values'][3][9]
#                 },
#                 {
#                     'date': result_first['values'][0][10],
#                     'value': result_first['values'][3][10]
#                 },
#                 {
#                     'date': result_first['values'][0][11],
#                     'value': result_first['values'][3][11]
#                 },
#                 {
#                     'date': result_first['values'][0][12],
#                     'value': result_first['values'][3][12]
#                 },
#                 {
#                     'date': result_first['values'][0][13],
#                     'value': result_first['values'][3][13]
#                 },
#                 {
#                     'date': result_first['values'][0][14],
#                     'value': result_first['values'][3][14]
#                 },
#                 {
#                     'date': result_first['values'][0][15],
#                     'value': result_first['values'][3][15]
#                 },
#             ]
#         },
#     ]
# }
#
# manufactured_product = {
#     result_first['values'][7][1]: [
#         {
#             'name': result_first['values'][8][1],
#             'dates': [
#                 {
#                     'date': result_first['values'][0][2],
#                     'value': result_first['values'][8][2]
#                 },
#                 {
#                     'date': result_first['values'][0][3],
#                     'value': result_first['values'][8][3]
#                 },
#                 {
#                     'date': result_first['values'][0][4],
#                     'value': result_first['values'][8][4]
#                 },
#                 {
#                     'date': result_first['values'][0][5],
#                     'value': result_first['values'][8][5]
#                 },
#                 {
#                     'date': result_first['values'][0][6],
#                     'value': result_first['values'][8][6]
#                 },
#                 {
#                     'date': result_first['values'][0][7],
#                     'value': result_first['values'][8][7]
#                 },
#                 {
#                     'date': result_first['values'][0][8],
#                     'value': result_first['values'][8][8]
#                 },
#                 {
#                     'date': result_first['values'][0][9],
#                     'value': result_first['values'][8][9]
#                 },
#                 {
#                     'date': result_first['values'][0][10],
#                     'value': result_first['values'][8][10]
#                 },
#                 {
#                     'date': result_first['values'][0][11],
#                     'value': result_first['values'][8][11]
#                 },
#                 {
#                     'date': result_first['values'][0][12],
#                     'value': result_first['values'][8][12]
#                 },
#                 {
#                     'date': result_first['values'][0][13],
#                     'value': result_first['values'][8][13]
#                 },
#                 {
#                     'date': result_first['values'][0][14],
#                     'value': result_first['values'][8][14]
#                 },
#                 {
#                     'date': result_first['values'][0][15],
#                     'value': result_first['values'][8][15]
#                 },
#             ]
#         },
#         {
#             'name': result_first['values'][9][1],
#             'dates': [
#                 {
#                     'date': result_first['values'][0][2],
#                     'value': result_first['values'][9][2]
#                 },
#                 {
#                     'date': result_first['values'][0][3],
#                     'value': result_first['values'][9][3]
#                 },
#                 {
#                     'date': result_first['values'][0][4],
#                     'value': result_first['values'][9][4]
#                 },
#                 {
#                     'date': result_first['values'][0][5],
#                     'value': result_first['values'][9][5]
#                 },
#                 {
#                     'date': result_first['values'][0][6],
#                     'value': result_first['values'][9][6]
#                 },
#                 {
#                     'date': result_first['values'][0][7],
#                     'value': result_first['values'][9][7]
#                 },
#                 {
#                     'date': result_first['values'][0][8],
#                     'value': result_first['values'][9][8]
#                 },
#                 {
#                     'date': result_first['values'][0][9],
#                     'value': result_first['values'][9][9]
#                 },
#                 {
#                     'date': result_first['values'][0][10],
#                     'value': result_first['values'][9][10]
#                 },
#                 {
#                     'date': result_first['values'][0][11],
#                     'value': result_first['values'][9][11]
#                 },
#                 {
#                     'date': result_first['values'][0][12],
#                     'value': result_first['values'][9][12]
#                 },
#                 {
#                     'date': result_first['values'][0][13],
#                     'value': result_first['values'][9][13]
#                 },
#                 {
#                     'date': result_first['values'][0][14],
#                     'value': result_first['values'][9][14]
#                 },
#                 {
#                     'date': result_first['values'][0][15],
#                     'value': result_first['values'][9][15]
#                 },
#             ]
#         },
#         {
#             'name': result_first['values'][10][1],
#             'dates': [
#                 {
#                     'date': result_first['values'][0][2],
#                     'value': result_first['values'][10][2]
#                 },
#                 {
#                     'date': result_first['values'][0][3],
#                     'value': result_first['values'][10][3]
#                 },
#                 {
#                     'date': result_first['values'][0][4],
#                     'value': result_first['values'][10][4]
#                 },
#                 {
#                     'date': result_first['values'][0][5],
#                     'value': result_first['values'][10][5]
#                 },
#                 {
#                     'date': result_first['values'][0][6],
#                     'value': result_first['values'][10][6]
#                 },
#                 {
#                     'date': result_first['values'][0][7],
#                     'value': result_first['values'][10][7]
#                 },
#                 {
#                     'date': result_first['values'][0][8],
#                     'value': result_first['values'][10][8]
#                 },
#                 {
#                     'date': result_first['values'][0][9],
#                     'value': result_first['values'][10][9]
#                 },
#                 {
#                     'date': result_first['values'][0][10],
#                     'value': result_first['values'][10][10]
#                 },
#                 {
#                     'date': result_first['values'][0][11],
#                     'value': result_first['values'][10][11]
#                 },
#                 {
#                     'date': result_first['values'][0][12],
#                     'value': result_first['values'][10][12]
#                 },
#                 {
#                     'date': result_first['values'][0][13],
#                     'value': result_first['values'][10][13]
#                 },
#                 {
#                     'date': result_first['values'][0][14],
#                     'value': result_first['values'][10][14]
#                 },
#                 {
#                     'date': result_first['values'][0][15],
#                     'value': result_first['values'][10][15]
#                 },
#
#             ]
#         },
#     ]
# }
#
# accepted_product = {
#     result_first['values'][11][1]: [
#         {
#             'name': result_first['values'][12][1],
#             'dates': [
#                 {
#                     'date': result_first['values'][0][2],
#                     'value': result_first['values'][12][2]
#                 },
#                 {
#                     'date': result_first['values'][0][3],
#                     'value': result_first['values'][12][3]
#                 },
#                 {
#                     'date': result_first['values'][0][4],
#                     'value': result_first['values'][12][4]
#                 },
#                 {
#                     'date': result_first['values'][0][5],
#                     'value': result_first['values'][12][5]
#                 },
#                 {
#                     'date': result_first['values'][0][6],
#                     'value': result_first['values'][12][6]
#                 },
#                 {
#                     'date': result_first['values'][0][7],
#                     'value': result_first['values'][12][7]
#                 },
#                 {
#                     'date': result_first['values'][0][8],
#                     'value': result_first['values'][12][8]
#                 },
#                 {
#                     'date': result_first['values'][0][9],
#                     'value': result_first['values'][12][9]
#                 },
#                 {
#                     'date': result_first['values'][0][10],
#                     'value': result_first['values'][12][10]
#                 },
#                 {
#                     'date': result_first['values'][0][11],
#                     'value': result_first['values'][12][11]
#                 },
#                 {
#                     'date': result_first['values'][0][12],
#                     'value': result_first['values'][12][12]
#                 },
#                 {
#                     'date': result_first['values'][0][13],
#                     'value': result_first['values'][12][13]
#                 },
#                 {
#                     'date': result_first['values'][0][14],
#                     'value': result_first['values'][12][14]
#                 },
#                 {
#                     'date': result_first['values'][0][15],
#                     'value': result_first['values'][12][15]
#                 },
#             ]
#         },
#         {
#             'name': result_first['values'][13][1],
#             'dates': [
#                 {
#                     'date': result_first['values'][0][2],
#                     'value': result_first['values'][13][2]
#                 },
#                 {
#                     'date': result_first['values'][0][3],
#                     'value': result_first['values'][13][3]
#                 },
#                 {
#                     'date': result_first['values'][0][4],
#                     'value': result_first['values'][13][4]
#                 },
#                 {
#                     'date': result_first['values'][0][5],
#                     'value': result_first['values'][13][5]
#                 },
#                 {
#                     'date': result_first['values'][0][6],
#                     'value': result_first['values'][13][6]
#                 },
#                 {
#                     'date': result_first['values'][0][7],
#                     'value': result_first['values'][13][7]
#                 },
#                 {
#                     'date': result_first['values'][0][8],
#                     'value': result_first['values'][13][8]
#                 },
#                 {
#                     'date': result_first['values'][0][9],
#                     'value': result_first['values'][13][9]
#                 },
#                 {
#                     'date': result_first['values'][0][10],
#                     'value': result_first['values'][13][10]
#                 },
#                 {
#                     'date': result_first['values'][0][11],
#                     'value': result_first['values'][13][11]
#                 },
#                 {
#                     'date': result_first['values'][0][12],
#                     'value': result_first['values'][13][12]
#                 },
#                 {
#                     'date': result_first['values'][0][13],
#                     'value': result_first['values'][13][13]
#                 },
#                 {
#                     'date': result_first['values'][0][14],
#                     'value': result_first['values'][13][14]
#                 },
#                 {
#                     'date': result_first['values'][0][15],
#                     'value': result_first['values'][13][15]
#                 },
#             ]
#         },
#         {
#             'name': result_first['values'][14][1],
#             'dates': [
#                 {
#                     'date': result_first['values'][0][2],
#                     'value': result_first['values'][14][2]
#                 },
#                 {
#                     'date': result_first['values'][0][3],
#                     'value': result_first['values'][14][3]
#                 },
#                 {
#                     'date': result_first['values'][0][4],
#                     'value': result_first['values'][14][4]
#                 },
#                 {
#                     'date': result_first['values'][0][5],
#                     'value': result_first['values'][14][5]
#                 },
#                 {
#                     'date': result_first['values'][0][6],
#                     'value': result_first['values'][14][6]
#                 },
#                 {
#                     'date': result_first['values'][0][7],
#                     'value': result_first['values'][14][7]
#                 },
#                 {
#                     'date': result_first['values'][0][8],
#                     'value': result_first['values'][14][8]
#                 },
#                 {
#                     'date': result_first['values'][0][9],
#                     'value': result_first['values'][14][9]
#                 },
#                 {
#                     'date': result_first['values'][0][10],
#                     'value': result_first['values'][14][10]
#                 },
#                 {
#                     'date': result_first['values'][0][11],
#                     'value': result_first['values'][14][11]
#                 },
#                 {
#                     'date': result_first['values'][0][12],
#                     'value': result_first['values'][14][12]
#                 },
#                 {
#                     'date': result_first['values'][0][13],
#                     'value': result_first['values'][14][13]
#                 },
#                 {
#                     'date': result_first['values'][0][14],
#                     'value': result_first['values'][14][14]
#                 },
#                 {
#                     'date': result_first['values'][0][15],
#                     'value': result_first['values'][14][15]
#                 },
#
#             ]
#         },
#     ]
# }
#
# defective_products = {
#     result_first['values'][16][11]: [
#         {
#             'name': result_first['values'][17][1],
#             'dates': [
#                 {
#                     'date': result_first['values'][0][2],
#                     'value': result_first['values'][17][2]
#                 },
#                 {
#                     'date': result_first['values'][0][3],
#                     'value': result_first['values'][17][3]
#                 },
#                 {
#                     'date': result_first['values'][0][4],
#                     'value': result_first['values'][17][4]
#                 },
#                 {
#                     'date': result_first['values'][0][5],
#                     'value': result_first['values'][17][5]
#                 },
#                 {
#                     'date': result_first['values'][0][6],
#                     'value': result_first['values'][17][6]
#                 },
#                 {
#                     'date': result_first['values'][0][7],
#                     'value': result_first['values'][17][7]
#                 },
#                 {
#                     'date': result_first['values'][0][8],
#                     'value': result_first['values'][17][8]
#                 },
#                 {
#                     'date': result_first['values'][0][9],
#                     'value': result_first['values'][17][9]
#                 },
#                 {
#                     'date': result_first['values'][0][10],
#                     'value': result_first['values'][17][10]
#                 },
#                 {
#                     'date': result_first['values'][0][11],
#                     'value': result_first['values'][17][11]
#                 },
#                 {
#                     'date': result_first['values'][0][12],
#                     'value': result_first['values'][17][12]
#                 },
#                 {
#                     'date': result_first['values'][0][13],
#                     'value': result_first['values'][17][13]
#                 },
#                 {
#                     'date': result_first['values'][0][14],
#                     'value': result_first['values'][17][14]
#                 },
#                 {
#                     'date': result_first['values'][0][15],
#                     'value': result_first['values'][17][15]
#                 },
#             ]
#         },
#     ]
# }
