from django.urls import path
from rest_framework import routers

from core.api_views import GoogleSheetView, MachineId, MachineView, NewsView
from core.views import GoogleSheetsView, NewsListView

router = routers.SimpleRouter()
router.register('api/v1/google-sheets', GoogleSheetView)
router.register('api/v1/machines', MachineView)
router.register('api/v1/news', NewsView)

urlpatterns = [
    # path('', HomeView.as_view(), name='home'),
    path('', GoogleSheetsView.as_view(), name='google-sheet'),
    path('news', NewsListView.as_view(), name='news'),
    path('api/v1/machine_id/', MachineId.as_view())
]
