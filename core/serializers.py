from rest_framework import serializers

from core.models import GoogleSheetModel, Machine, News


class GoogleSheetSerializer(serializers.ModelSerializer):
    class Meta:
        model = GoogleSheetModel
        fields = '__all__'


class MachineSerializer(serializers.ModelSerializer):
    class Meta:
        model = Machine
        fields = '__all__'


class NewsSerializer(serializers.ModelSerializer):
    class Meta:
        model = News
        fields = '__all__'
