from ckeditor_uploader.fields import RichTextUploadingField
from django.db import models

from core.choices import NEWS_TYPE_CHOICES


class GoogleSheetModel(models.Model):
    row1 = models.TextField(verbose_name='Первая колонка',
                            blank=True, null=True)
    row2 = models.TextField(verbose_name='Вторая колонка',
                            blank=True, null=True)
    row3 = models.TextField(verbose_name='Третья колонка',
                            blank=True, null=True)
    row4 = models.TextField(verbose_name='Четвертая колонка',
                            blank=True, null=True)

    def __str__(self):
        return self.row1


class Machine(models.Model):
    name = models.CharField(max_length=255,
                            verbose_name='Название')
    ip = models.CharField(max_length=255,
                          verbose_name='IP')
    field_day = models.CharField(max_length=255,
                                 verbose_name='День',
                                 blank=True, null=True)
    field_night = models.CharField(max_length=255,
                                   verbose_name='Ночь',
                                   blank=True, null=True)

    def __str__(self):
        return self.name


class News(models.Model):
    machine_id = models.ForeignKey('core.Machine',
                                   on_delete=models.CASCADE)
    file = models.FileField(upload_to='file',
                            blank=True, null=True)
    news_type = models.CharField(max_length=255,
                                 verbose_name='Тип',
                                 blank=True, null=True,
                                 choices=NEWS_TYPE_CHOICES)
    youtube_url = models.CharField(max_length=255,
                                   verbose_name='Адрес ютуба',
                                   blank=True, null=True)
    description = RichTextUploadingField(verbose_name='Описание',
                                         blank=True, null=True)
    interval = models.IntegerField(default=0)
    status = models.BooleanField(default=False,
                                 verbose_name='Статус')


class Setting(models.Model):
    interval = models.IntegerField(default=0)

    def __str__(self):
        return str(self.interval)
