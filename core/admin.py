from django.contrib import admin

from core.models import GoogleSheetModel, Machine, News, Setting


@admin.register(GoogleSheetModel)
class GoogleSheetModelAdmin(admin.ModelAdmin):
    pass


@admin.register(Machine)
class MachineAdmin(admin.ModelAdmin):
    pass


@admin.register(News)
class NewsAdmin(admin.ModelAdmin):
    pass


@admin.register(Setting)
class SettingAdmin(admin.ModelAdmin):
    pass
