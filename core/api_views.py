from django.http import JsonResponse
from rest_framework import viewsets
from rest_framework.permissions import AllowAny
from rest_framework.views import APIView

from core.models import GoogleSheetModel, Machine, News
from core.serializers import GoogleSheetSerializer, MachineSerializer, NewsSerializer


class GoogleSheetView(viewsets.ModelViewSet):
    queryset = GoogleSheetModel.objects.all()
    serializer_class = GoogleSheetSerializer
    http_method_names = ['get']
    permission_classes = [AllowAny]


class MachineView(viewsets.ModelViewSet):
    queryset = Machine.objects.all()
    serializer_class = MachineSerializer
    http_method_names = ['get']
    permission_classes = [AllowAny]


class NewsView(viewsets.ModelViewSet):
    queryset = News.objects.all()
    serializer_class = NewsSerializer
    http_method_names = ['get']
    permission_classes = [AllowAny]


IP_ACCESSES = {
    '192.168.0.101': '5',
    '192.168.0.102': '4',
    '192.168.0.103': '3',
    '192.168.0.104': '2',
}


class MachineId(APIView):

    def get(self, request):
        return JsonResponse(IP_ACCESSES)
